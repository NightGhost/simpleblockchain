﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace SimpleBlockchain.Crypto.Signatures
{
    class SignatureProvider : ISignatureProvider
    {
        private ECDsaCng innerSigner;
        
        public byte[] PublicKey { get; }

        public SignatureProvider()
        {
            innerSigner = new ECDsaCng();
            PublicKey = innerSigner.Key.Export(CngKeyBlobFormat.EccPublicBlob);
        }

        public byte[] SignHash(byte[] hash) => innerSigner.SignHash(hash);
    }
}
