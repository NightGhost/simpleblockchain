﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBlockchain.Crypto.Signatures
{
    public class ECDSAFactory : ISignatureFactory
    {
        private Lazy<SignatureVerifier> signatureVerifier;

        public ECDSAFactory()
        {
            signatureVerifier = new Lazy<SignatureVerifier>(() => new SignatureVerifier());
        }

        public ISignatureProvider GetSignatureProvider() => new SignatureProvider();

        public ISignatureVerifier GetSignatureVerifier() => signatureVerifier.Value;
    }
}
