﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace SimpleBlockchain.Crypto.Hash
{
    public class RandomNonceGenerator : INonceGenerator
    {
        private byte[] currentNonce;
        private Random random;

        public int NonceLength { get; set; }

        public RandomNonceGenerator(int nonceLength)
        {
            NonceLength = nonceLength;
            currentNonce = new byte[NonceLength];
            random = new Random(newSeed());
        }
        
        private int newSeed() => DateTime.Now.Millisecond + DateTime.Now.Month + DateTime.Now.Second + DateTime.Now.DayOfYear;

        // Add normal generation.
        public byte[] GetNextNonce()
        {
            currentNonce = new byte[NonceLength];
            random.NextBytes(currentNonce);

            return currentNonce;
        }

        public void Reset() => random = new Random(newSeed());
    }
}
