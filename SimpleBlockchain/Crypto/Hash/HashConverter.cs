﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBlockchain.Crypto.Hash
{
    class HashConverter : IByteConverter
    {
        public string ConvertToString(byte[] sequence) => String.Join(String.Empty, sequence.Select(number => number.ToString("x")));
    }
}
