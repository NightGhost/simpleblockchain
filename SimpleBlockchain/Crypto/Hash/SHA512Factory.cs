﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBlockchain.Crypto.Hash
{
    class SHA512Factory : IHashFactory
    {
        private Lazy<SHA512Digest> SHA512Digest;
        private Lazy<HashConverter> hashConverter;
        private Lazy<BigIntegerNonceGenerator> nonceGenerator;

        public SHA512Factory()
        {
            SHA512Digest = new Lazy<SHA512Digest>(() => new SHA512Digest());
            hashConverter = new Lazy<HashConverter>(() => new HashConverter());
            nonceGenerator = new Lazy<BigIntegerNonceGenerator>(() => new BigIntegerNonceGenerator(16));
        }

        public IByteConverter GetByteConverter() => hashConverter.Value;

        public IDigest GetDigest() => SHA512Digest.Value;

        public INonceGenerator GetNonceGenerator() => nonceGenerator.Value;
    }
}
