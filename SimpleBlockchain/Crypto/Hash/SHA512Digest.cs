﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace SimpleBlockchain.Crypto.Hash
{
    class SHA512Digest : IDigest
    {
        private SHA512Managed digest;

        public int HashLength => digest.HashSize / 8;

        public SHA512Digest()
        {
            digest = new SHA512Managed();
        }

        public byte[] GetHash(byte[] data) => digest.ComputeHash(data);
    }
}
