﻿using System;
using System.Collections.Generic;
using SimpleBlockchain.BlockchainComponents;
using SimpleBlockchain.WalletComponents;
using SimpleBlockchain.NetworkComponents;
using SimpleBlockchain.Crypto.Hash;
using SimpleBlockchain.Crypto.Signatures;
using Newtonsoft.Json;

namespace SimpleBlockchain
{
    class Program
    {
        public const int Difficulty = 2;

        static void Main(string[] args)
        {
            IHashFactory hashFactory = new SHA512Factory();
            ISignatureFactory signatureFactory = new ECDSAFactory();

            Wallet firstWallet = new Wallet(signatureFactory, hashFactory);

            #region Initial block.

            Transaction initialTransaction = new Transaction(firstWallet.PublicKey, firstWallet.PublicKey, 64, hashFactory.GetNonceGenerator());
            LinkedList<Transaction> initialList = new LinkedList<Transaction>();

            initialTransaction.SignTransaction(firstWallet.Signer, hashFactory);
            initialList.AddLast(initialTransaction);

            Block initialBlock = new Block(firstWallet.PublicKey, new byte[hashFactory.GetDigest().HashLength], initialList, hashFactory);
            initialBlock.MineBlock(Difficulty, hashFactory);
            initialBlock.SignBlock(firstWallet.Signer, hashFactory);

            #endregion

            Blockchain firstBlockchain = new Blockchain(firstWallet, hashFactory, signatureFactory, Difficulty, initialBlock);
            Wallet secondWallet = new Wallet(signatureFactory, hashFactory);
            Blockchain secondBlockchain = new Blockchain(secondWallet, hashFactory, signatureFactory, Difficulty, initialBlock);

            #region Network.

            IBroadcaster firstToSecond = new Network(secondBlockchain);
            IBroadcaster secondToFirst = new Network(firstBlockchain);

            firstBlockchain.NetworkBroadcaster = firstToSecond;
            secondBlockchain.NetworkBroadcaster = secondToFirst;

            #endregion

            firstWallet.Blockchain = firstBlockchain;
            secondWallet.Blockchain = secondBlockchain;

            firstWallet.SendTokens(16, secondWallet.PublicKey);
            secondBlockchain.ProcessPendingTransactions();

            secondWallet.SendTokens(8, firstWallet.PublicKey);
            firstBlockchain.ProcessPendingTransactions();

            Console.WriteLine("First blockchain:\n\n{");

            foreach (Block block in firstBlockchain)
            {
                Console.WriteLine(JsonConvert.SerializeObject(block, Formatting.Indented));
                Console.WriteLine();
            }

            Console.WriteLine("}\n");

            Console.WriteLine("Second blockchain:\n\n{");

            foreach (Block block in secondBlockchain)
            {
                Console.WriteLine(JsonConvert.SerializeObject(block, Formatting.Indented));
                Console.WriteLine();
            }

            Console.WriteLine("}\n");

            Console.WriteLine(firstBlockchain.IsValid ? "First blockchain is valid" : "First blockchain is invalid");
            Console.WriteLine(firstBlockchain.IsValid ? "Second blockchain is valid" : "Second blockchain is invalid");

            Console.WriteLine($"Cryptocurrency ownership for first wallet: {firstBlockchain.CountBalanceForWallet(firstWallet.PublicKey)}");
            Console.WriteLine($"Cryptocurrency ownership for second wallet: {firstBlockchain.CountBalanceForWallet(secondWallet.PublicKey)}");
        }
    }
}
