﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleBlockchain.Crypto.Hash;
using SimpleBlockchain.Crypto.Signatures;

namespace SimpleBlockchain.WalletComponents
{
    public class Transaction
    {
        public byte[] Nonce { get; private set; }

        public byte[] Sender { get; }
        public byte[] Recipient { get; }

        // Change to BigInteger.
        public int Amount { get; }
        public byte[] Signature { get; private set; }

        public Transaction(byte[] sender, byte[] recipient, int amount, INonceGenerator nonceGenerator)
        {
            Sender = sender;
            Recipient = recipient;

            Nonce = nonceGenerator.GetNextNonce();
            Amount = amount;
        }
        
        public byte[] ComputeHash(IHashFactory hashFactory)
        {
            IByteConverter converter = hashFactory.GetByteConverter();
            string rawData = converter.ConvertToString(Nonce) + converter.ConvertToString(Sender) + converter.ConvertToString(Recipient) + Amount;
            byte[] data = Encoding.ASCII.GetBytes(rawData);

            return hashFactory.GetDigest().GetHash(data);
        }

        public void SignTransaction(ISignatureProvider signer, IHashFactory hashFactory)
        {
            byte[] hash = ComputeHash(hashFactory);

            Signature = signer.SignHash(hash);
        }

        public bool VerifyTransactionSignature(ISignatureVerifier verifier, IHashFactory hashFactory)
        {
            byte[] hash = ComputeHash(hashFactory);

            return verifier.VerifyHash(Sender, hash, Signature);
        }
    }
}
