﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SimpleBlockchain.WalletComponents;
using SimpleBlockchain.NetworkComponents;
using SimpleBlockchain.Crypto.Hash;
using SimpleBlockchain.Crypto.Signatures;

namespace SimpleBlockchain.BlockchainComponents
{
    // Hide this class behind IBlockchain interface.
    public class Blockchain : IEnumerable<Block>
    {
        public Wallet UserWallet { get; }
        public int Difficulty { get; set; }

        private LinkedList<Block> innerChain;
        private LinkedList<Transaction> pendingTransactions;

        public ISignatureFactory SignatureFactory { get; set; }
        public ISignatureVerifier Verifier { get; }

        public IHashFactory HashFactory { get; set; }
        public IDigest Digest { get; }
        public IByteConverter Converter { get; }
        public INonceGenerator NonceGenerator { get; }

        public IBroadcaster NetworkBroadcaster { get; set; }
        
        public bool IsValid
        {
            get
            {
                byte[] target = new byte[Difficulty];
                byte[] previousHash = new byte[Digest.HashLength];
                Block firstBlock = innerChain.First.Value;

                Array.Copy(firstBlock.PreviousHash, previousHash, Digest.HashLength);

                foreach (Block block in innerChain)
                {
                    if (!validateBlock(block, previousHash, target))
                        return false;

                    Array.Copy(block.Hash, previousHash, Digest.HashLength);
                }

                return true;
            }
        }

        public Blockchain(Wallet userWallet, IHashFactory hashFactory, ISignatureFactory signatureFactory, int difficulty, Block initialBlock)
        {
            SignatureFactory = signatureFactory;
            Verifier = signatureFactory.GetSignatureVerifier();

            HashFactory = hashFactory;
            Digest = HashFactory.GetDigest();
            Converter = HashFactory.GetByteConverter();
            NonceGenerator = HashFactory.GetNonceGenerator();

            UserWallet = userWallet;
            Difficulty = difficulty;

            pendingTransactions = new LinkedList<Transaction>();
            innerChain = new LinkedList<Block>();

            NonceGenerator.Reset();

            if (!validateBlock(initialBlock, new byte[Digest.HashLength], new byte[difficulty]))
                throw new ArgumentException("Genesis block is invalid");

            innerChain.AddLast(initialBlock);
            UserWallet.AcceptTransactions(initialBlock.Transactions.ToArray());
        }

        private void addBlock()
        {
            Block lastBlock = innerChain.Last.Value;
            Block block = new Block(UserWallet.PublicKey, lastBlock.Hash, pendingTransactions, HashFactory);

            NonceGenerator.Reset();
            block.MineBlock(Difficulty, HashFactory);
            block.SignBlock(UserWallet.Signer, HashFactory);

            innerChain.AddLast(block);
            UserWallet.AcceptTransactions(block.Transactions.ToArray());
        }
        
        private bool validateTransactionAmount(Transaction transaction) => CountBalanceForWallet(transaction.Sender) >= transaction.Amount;

        private bool validateTransactionSignature(Transaction transaction) => transaction.VerifyTransactionSignature(Verifier, HashFactory);

        private bool validateBlock(Block block, byte[] previousHash, byte[] target)
        {
            if (!block.PreviousHash.SequenceEqual(previousHash))
                return false;

            if (!block.Hash.SequenceEqual(block.ComputeHash(HashFactory)))
                return false;

            if (!block.Hash.Take(Difficulty).SequenceEqual(target))
                return false;

            if (!block.Transactions.All(transaction => validateTransactionSignature(transaction)))
                return false;

            if (!block.VerifyBlockSignature(Verifier, HashFactory))
                return false;

            return true;
        }

        public int CountBalanceForWallet(byte[] address)
        {
            int total = 0;

            foreach (Block block in innerChain)
                foreach (Transaction transaction in block.Transactions)
                {
                    if (transaction.Recipient.SequenceEqual(address))
                    {
                        total += transaction.Amount;

                        continue;
                    }

                    if (transaction.Sender.SequenceEqual(address) && !transaction.Recipient.SequenceEqual(address))
                        total -= transaction.Amount;
                }

            return total;
        }
        
        public void AcceptTransaction(Transaction transaction)
        {
            if (!validateTransactionSignature(transaction))
                throw new ArgumentException("Transaction signature is invalid");

            if (!validateTransactionAmount(transaction))
                throw new ArgumentException("You don't have enough tokens on your cash");

            pendingTransactions.AddLast(transaction);
        }

        public void AcceptBlock(Block block)
        {
            byte[] previousHash = innerChain.Last.Value.Hash;

            if (!validateBlock(block, previousHash, new byte[Difficulty]))
                throw new ArgumentException("Block is invalid");

            innerChain.AddLast(block);
            pendingTransactions = new LinkedList<Transaction>();
        }

        public void AddNewTransaction(Transaction transaction)
        {
            AcceptTransaction(transaction);
            NetworkBroadcaster?.BroadcastTransaction(transaction);
        }

        public void ProcessPendingTransactions()
        {
            addBlock();
            NetworkBroadcaster?.BroadcastBlock(innerChain.Last.Value);
            pendingTransactions = new LinkedList<Transaction>();
            
            Transaction reward = new Transaction(UserWallet.PublicKey, UserWallet.PublicKey, 15, NonceGenerator);

            reward.SignTransaction(UserWallet.Signer, HashFactory);
            AddNewTransaction(reward);
        }

        public IEnumerator<Block> GetEnumerator() => innerChain.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
