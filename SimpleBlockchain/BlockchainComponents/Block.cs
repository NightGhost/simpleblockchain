﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleBlockchain.WalletComponents;
using SimpleBlockchain.Crypto.Hash;
using SimpleBlockchain.Crypto.Signatures;

namespace SimpleBlockchain.BlockchainComponents
{
    // Hide this class behind IBlock interface.
    public class Block
    {
        public byte[] MinerAddress { get; }
        public byte[] MinerSignature { get; private set; }

        public byte[] Nonce { get; private set; }
        public byte[] Hash { get; private set; }
        public byte[] PreviousHash { get; }
        public ICollection<Transaction> Transactions { get; }
        public DateTime CreationTime { get; }

        public Block(byte[] minerAddress, byte[] previousHash, ICollection<Transaction> transactions, IHashFactory hashFactory)
        {
            MinerAddress = minerAddress;

            Transactions = transactions;
            CreationTime = DateTime.Now;

            IDigest digest = hashFactory.GetDigest();
            INonceGenerator nonceGenerator = hashFactory.GetNonceGenerator();

            PreviousHash = new byte[digest.HashLength];
            Array.Copy(previousHash, PreviousHash, digest.HashLength);

            Nonce = nonceGenerator.GetNextNonce();
            Hash = ComputeHash(hashFactory);
        }

        // Encapsulate algorithm into separate class.
        public byte[] ComputeHash(IHashFactory hashFactory)
        {
            IByteConverter converter = hashFactory.GetByteConverter();
            IDigest digest = hashFactory.GetDigest();

            byte[] transactionHash = Transactions.Select(transaction => transaction.ComputeHash(hashFactory)).Aggregate((first, second) =>
            {
                string strData = converter.ConvertToString(first) + converter.ConvertToString(second);
                byte[] byteData = Encoding.ASCII.GetBytes(strData);

                return digest.GetHash(byteData);
            });

            string data = converter.ConvertToString(PreviousHash) +
                          CreationTime.ToString() +
                          converter.ConvertToString(Nonce) +
                          converter.ConvertToString(transactionHash) +
                          converter.ConvertToString(MinerAddress);

            byte[] rawData = Encoding.ASCII.GetBytes(data);

            return digest.GetHash(rawData);
        }

        // Use Strategy pattern for mining algorithm.
        public void MineBlock(int difficulty, IHashFactory hashFactory)
        {
            IDigest digest = hashFactory.GetDigest();
            INonceGenerator nonceGenerator = hashFactory.GetNonceGenerator();

            if (difficulty >= digest.HashLength)
                throw new ArgumentException("Difficulty can not be greater or equal to hash length.");

            if (difficulty <= 0)
                throw new ArgumentException("Difficulty can not be greater or equal to zero.");

            byte[] target = new byte[difficulty];

            while (!Hash.Take(difficulty).SequenceEqual(target))
            {
                Nonce = nonceGenerator.GetNextNonce();
                Hash = ComputeHash(hashFactory);
            }
        }

        public void SignBlock(ISignatureProvider signer, IHashFactory hashFactory)
        {
            byte[] hash = ComputeHash(hashFactory);

            MinerSignature = signer.SignHash(hash);
        }

        public bool VerifyBlockSignature(ISignatureVerifier verifier, IHashFactory hashFactory)
        {
            byte[] hash = ComputeHash(hashFactory);

            return verifier.VerifyHash(MinerAddress, hash, MinerSignature);
        }
    }
}
