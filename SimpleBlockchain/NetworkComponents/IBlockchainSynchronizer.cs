﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBlockchain.NetworkComponents
{
    public interface IBlockchainSynchronizer
    {
        // Address is someone's public key.
        void SynchronizeBlockchainWith(byte[] address);
    }
}
